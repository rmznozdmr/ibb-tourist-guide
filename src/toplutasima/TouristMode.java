/*
 * Created by JFormDesigner on Tue Dec 15 19:37:46 EET 2015
 */

package toplutasima;

import java.awt.*;
import java.awt.event.*;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.border.*;

/**
 * @author ramazan ozdemir
 */
public class TouristMode extends JFrame {
    private Kullanici myturist;

    private  String[] temp = new String[15] ;
    public TouristMode() {
        myturist= new Kullanici();
        myturist.tourist();
        for(int i=0;i<myturist.getTURİSTTİK_YERLER().size();i++)
            temp[i]=myturist.getTURİSTTİK_YERLER().get(i);
        initComponents();

        historicalPlaces.setModel(new DefaultComboBoxModel<>(temp));

    }


    private void okButtonActionPerformed(ActionEvent e) {
        // TODO add your code here
        try {
            resultLabel.setText(myturist.YOL(location.getText(), myturist.tarihiyerDURAGI((String) historicalPlaces.getSelectedItem())));
        }
        catch (NullPointerException exception){
            resultLabel.setText("Bu merkeze burdan ulaşım yoktur");
        }
    }

    private void cancelButtonActionPerformed(ActionEvent e) {
        // TODO add your code here
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - ramazan ozdemir
        dialogPane = new JPanel();
        contentPanel = new JPanel();
        historicalPlaces = new JComboBox<>();
        location = new JTextField();
        label1 = new JLabel();
        resultLabel = new JLabel();
        buttonBar = new JPanel();
        okButton = new JButton();
        cancelButton = new JButton();

        //======== this ========
        setBackground(Color.green);
        setForeground(Color.black);
        Container contentPane = getContentPane();
        contentPane.setLayout(new BorderLayout());

        //======== dialogPane ========
        {
            dialogPane.setBorder(new EmptyBorder(12, 12, 12, 12));
            dialogPane.setBackground(new Color(204, 255, 204));
            dialogPane.setForeground(Color.black);

            // JFormDesigner evaluation mark
            dialogPane.setBorder(new javax.swing.border.CompoundBorder(
                new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                    "JFormDesigner Evaluation", javax.swing.border.TitledBorder.CENTER,
                    javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                    java.awt.Color.red), dialogPane.getBorder())); dialogPane.addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

            dialogPane.setLayout(new BorderLayout());

            //======== contentPanel ========
            {
                contentPanel.setAlignmentX(-0.5F);
                contentPanel.setBackground(new Color(204, 255, 204));
                contentPanel.setForeground(Color.black);

                //---- historicalPlaces ----
                historicalPlaces.setFont(new Font("Dialog", Font.BOLD, 20));
                historicalPlaces.setMaximumRowCount(12);
                historicalPlaces.setModel(new DefaultComboBoxModel<>(new String[] {
                    "model1",
                    "model2"
                }));
                historicalPlaces.setAlignmentX(-0.5F);
                historicalPlaces.setBackground(new Color(204, 255, 204));
                historicalPlaces.setForeground(new Color(102, 102, 0));

                //---- location ----
                location.setFont(new Font("Dialog", Font.BOLD, 20));
                location.setAlignmentX(-0.5F);
                location.setBackground(new Color(204, 255, 204));
                location.setForeground(new Color(102, 102, 0));

                //---- label1 ----
                label1.setText("Choice a Place And Enter Location");
                label1.setFont(new Font("Dialog", Font.PLAIN, 20));
                label1.setAlignmentX(-0.5F);
                label1.setBackground(new Color(204, 255, 204));
                label1.setForeground(new Color(102, 102, 0));

                //---- resultLabel ----
                resultLabel.setBackground(new Color(204, 255, 204));
                resultLabel.setForeground(new Color(153, 153, 0));
                resultLabel.setFont(new Font("Bitstream Charter", Font.BOLD, 16));

                GroupLayout contentPanelLayout = new GroupLayout(contentPanel);
                contentPanel.setLayout(contentPanelLayout);
                contentPanelLayout.setHorizontalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 383, GroupLayout.PREFERRED_SIZE)
                            .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addComponent(location)
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addComponent(historicalPlaces, GroupLayout.PREFERRED_SIZE, 355, GroupLayout.PREFERRED_SIZE)
                                    .addGap(0, 0, Short.MAX_VALUE)))
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(resultLabel, GroupLayout.PREFERRED_SIZE, 309, GroupLayout.PREFERRED_SIZE)
                            .addGap(34, 34, 34))
                );
                contentPanelLayout.setVerticalGroup(
                    contentPanelLayout.createParallelGroup()
                        .addGroup(contentPanelLayout.createSequentialGroup()
                            .addGap(8, 8, 8)
                            .addComponent(label1, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 9, Short.MAX_VALUE)
                            .addGroup(contentPanelLayout.createParallelGroup()
                                .addGroup(contentPanelLayout.createSequentialGroup()
                                    .addComponent(historicalPlaces, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                                    .addPreferredGap(LayoutStyle.ComponentPlacement.UNRELATED)
                                    .addComponent(location, GroupLayout.PREFERRED_SIZE, 70, GroupLayout.PREFERRED_SIZE)
                                    .addGap(34, 34, 34))
                                .addGroup(GroupLayout.Alignment.TRAILING, contentPanelLayout.createSequentialGroup()
                                    .addComponent(resultLabel, GroupLayout.PREFERRED_SIZE, 172, GroupLayout.PREFERRED_SIZE)
                                    .addGap(14, 14, 14))))
                );
            }
            dialogPane.add(contentPanel, BorderLayout.CENTER);

            //======== buttonBar ========
            {
                buttonBar.setBorder(new EmptyBorder(12, 0, 0, 0));
                buttonBar.setBackground(new Color(204, 255, 204));
                buttonBar.setForeground(Color.black);
                buttonBar.setLayout(new GridBagLayout());
                ((GridBagLayout)buttonBar.getLayout()).columnWidths = new int[] {0, 85, 80};
                ((GridBagLayout)buttonBar.getLayout()).columnWeights = new double[] {1.0, 0.0, 0.0};

                //---- okButton ----
                okButton.setText("OK");
                okButton.setAlignmentX(-0.5F);
                okButton.setBackground(new Color(204, 255, 204));
                okButton.setForeground(Color.black);
                okButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        okButtonActionPerformed(e);
                    }
                });
                buttonBar.add(okButton, new GridBagConstraints(1, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 5), 0, 0));

                //---- cancelButton ----
                cancelButton.setText("Cancel");
                cancelButton.setAlignmentX(-0.5F);
                cancelButton.setBackground(new Color(204, 255, 204));
                cancelButton.setForeground(Color.black);
                cancelButton.addActionListener(new ActionListener() {
                    @Override
                    public void actionPerformed(ActionEvent e) {
                        cancelButtonActionPerformed(e);
                    }
                });
                buttonBar.add(cancelButton, new GridBagConstraints(2, 0, 1, 1, 0.0, 0.0,
                    GridBagConstraints.CENTER, GridBagConstraints.BOTH,
                    new Insets(0, 0, 0, 0), 0, 0));
            }
            dialogPane.add(buttonBar, BorderLayout.SOUTH);
        }
        contentPane.add(dialogPane, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - ramazan ozdemir
    private JPanel dialogPane;
    private JPanel contentPanel;
    private JComboBox<String> historicalPlaces;
    private JTextField location;
    private JLabel label1;
    private JLabel resultLabel;
    private JPanel buttonBar;
    private JButton okButton;
    private JButton cancelButton;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
