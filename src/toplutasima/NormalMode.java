/*
 * Created by JFormDesigner on Tue Dec 15 18:45:05 EET 2015
 */

package toplutasima;

import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import javax.swing.*;
import javax.swing.GroupLayout;
import javax.swing.border.*;
import com.intellij.uiDesigner.core.*;
import com.jgoodies.forms.factories.*;
import com.jgoodies.forms.layout.*;
import info.clearthought.layout.*;
import org.jdesktop.swingx.border.*;

/**
 * @author ramazan ozdemir
 */
public class NormalMode extends JFrame {
    private Kullanici mynormal= new Kullanici();
    public NormalMode() {
        initComponents();
    }

    private void buttonOkActionPerformed(ActionEvent e) {
        // TODO add your code here

        //System.out.println(mynormal.YOL(locationArea.getText(),destinationArea.getText()));
        String result;
        try {
            result = mynormal.YOL(locationArea.getText(), destinationArea.getText());
        }
        catch (NullPointerException exception){
            mynormal.setLocation(locationArea.getText());
            mynormal.setDestination(destinationArea.getText());
            ArrayList<String> newresult = mynormal.varisSuresi("vapurlar");
            result="<html>";
            System.out.println(newresult.toString());
            for(int i=0;i<newresult.size();i++)
                result+="<br>" + newresult.get(i);
            result+="</html>";
        }

        resultLabel.setText(result);
    }

    private void cancelActionPerformed(ActionEvent e) {
        // TODO add your code here
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - ramazan ozdemir
        scrollPane1 = new JScrollPane();
        locationArea = new JTextArea();
        scrollPane2 = new JScrollPane();
        destinationArea = new JTextArea();
        location = new JLabel();
        destination = new JLabel();
        buttonOk = new JButton();
        resultLabel = new JLabel();
        button1 = new JButton();

        //======== this ========
        setForeground(new Color(0, 51, 51));
        setBackground(new Color(255, 255, 51));
        Container contentPane = getContentPane();

        //======== scrollPane1 ========
        {
            scrollPane1.setToolTipText("Location");
            scrollPane1.setViewportBorder(new BevelBorder(BevelBorder.LOWERED));
            scrollPane1.setForeground(new Color(0, 51, 51));
            scrollPane1.setBackground(new Color(255, 255, 51));

            //---- locationArea ----
            locationArea.setToolTipText("Location");
            locationArea.setFont(new Font("Monospaced", Font.PLAIN, 14));
            locationArea.setForeground(new Color(0, 51, 51));
            locationArea.setBackground(new Color(153, 153, 0));
            scrollPane1.setViewportView(locationArea);
        }

        //======== scrollPane2 ========
        {
            scrollPane2.setForeground(new Color(0, 51, 51));
            scrollPane2.setBackground(new Color(255, 255, 51));
            scrollPane2.setViewportBorder(new BevelBorder(BevelBorder.LOWERED));

            //---- destinationArea ----
            destinationArea.setFont(new Font("Monospaced", Font.PLAIN, 14));
            destinationArea.setForeground(new Color(0, 51, 51));
            destinationArea.setBackground(new Color(255, 255, 51));
            scrollPane2.setViewportView(destinationArea);
        }

        //---- location ----
        location.setText("Location");
        location.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
        location.setEnabled(false);
        location.setForeground(new Color(0, 51, 51));
        location.setLabelFor(locationArea);
        location.setBackground(new Color(255, 255, 51));

        //---- destination ----
        destination.setText("Destination");
        destination.setFont(new Font("Dialog", Font.BOLD | Font.ITALIC, 20));
        destination.setEnabled(false);
        destination.setForeground(new Color(0, 51, 51));
        destination.setLabelFor(destinationArea);
        destination.setBackground(new Color(255, 255, 51));
        destination.setToolTipText("Destination");

        //---- buttonOk ----
        buttonOk.setText("OK");
        buttonOk.setFont(new Font("Dialog", Font.BOLD, 18));
        buttonOk.setForeground(new Color(0, 51, 51));
        buttonOk.setBackground(new Color(255, 255, 51));
        buttonOk.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                buttonOkActionPerformed(e);
            }
        });

        //---- resultLabel ----
        resultLabel.setFont(new Font("Dialog", Font.ITALIC, 16));
        resultLabel.setForeground(new Color(0, 51, 51));
        resultLabel.setBackground(new Color(255, 255, 51));

        //---- button1 ----
        button1.setText("CANCEL");
        button1.setFont(new Font("Dialog", Font.BOLD, 14));
        button1.setForeground(new Color(0, 51, 51));
        button1.setBackground(new Color(255, 255, 51));
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelActionPerformed(e);
            }
        });

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup(GroupLayout.Alignment.TRAILING)
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(53, 53, 53)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(scrollPane2, GroupLayout.Alignment.TRAILING)
                        .addComponent(destination, GroupLayout.DEFAULT_SIZE, 223, Short.MAX_VALUE)
                        .addComponent(location, GroupLayout.Alignment.TRAILING, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(scrollPane1))
                    .addGap(63, 63, 63)
                    .addComponent(resultLabel, GroupLayout.PREFERRED_SIZE, 304, GroupLayout.PREFERRED_SIZE))
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap(408, Short.MAX_VALUE)
                    .addComponent(buttonOk, GroupLayout.PREFERRED_SIZE, 88, GroupLayout.PREFERRED_SIZE)
                    .addGap(31, 31, 31)
                    .addComponent(button1)
                    .addGap(24, 24, 24))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(34, 34, 34)
                    .addComponent(location, GroupLayout.PREFERRED_SIZE, 24, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addGroup(contentPaneLayout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                        .addGroup(contentPaneLayout.createSequentialGroup()
                            .addComponent(scrollPane1, GroupLayout.PREFERRED_SIZE, 39, GroupLayout.PREFERRED_SIZE)
                            .addGap(33, 33, 33)
                            .addComponent(destination, GroupLayout.PREFERRED_SIZE, 23, GroupLayout.PREFERRED_SIZE)
                            .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                            .addComponent(scrollPane2, GroupLayout.PREFERRED_SIZE, 35, GroupLayout.PREFERRED_SIZE))
                        .addComponent(resultLabel, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(18, 18, 18)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(button1, GroupLayout.PREFERRED_SIZE, 36, GroupLayout.PREFERRED_SIZE)
                        .addComponent(buttonOk, GroupLayout.PREFERRED_SIZE, 38, GroupLayout.PREFERRED_SIZE))
                    .addContainerGap(63, Short.MAX_VALUE))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - ramazan ozdemir
    private JScrollPane scrollPane1;
    private JTextArea locationArea;
    private JScrollPane scrollPane2;
    private JTextArea destinationArea;
    private JLabel location;
    private JLabel destination;
    private JButton buttonOk;
    private JLabel resultLabel;
    private JButton button1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
