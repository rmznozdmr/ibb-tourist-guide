package toplutasima;



/**
 * @author CAGLAR
 * @version 1.0
 * @created 13-Kas-2015 13:22:04
 */
public class Saat {

	private int dakikatKismi;
	private int saatKismi;

	public Saat(int saat,int dakika){
		this.saatKismi=saat;
		this.dakikatKismi=dakika;
	}

	public void finalize() throws Throwable {

	}

	public int getDakika(){
		return dakikatKismi;
	}

	public int getSaat(){
		return saatKismi;
	}

}