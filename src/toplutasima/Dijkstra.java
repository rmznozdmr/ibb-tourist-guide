package toplutasima;
import java.util.PriorityQueue;
import java.util.List;
import java.util.ArrayList;
import java.util.Collections;

class Vertex implements Comparable<Vertex>
{
    public final String name;
    public Edge[] adjacencies;
    public double minDistance = Double.POSITIVE_INFINITY;
    public Vertex previous;
    public Vertex(String argName) { name = argName; }
    public String toString() { return name; }
    public int compareTo(Vertex other)
    {
        return Double.compare(minDistance, other.minDistance);
    }

}


class Edge
{
    public final Vertex target;
    public final double weight;
    public Edge(Vertex argTarget, double argWeight)
    { target = argTarget; weight = argWeight; }
}

public class Dijkstra
{
    public static void computePaths(Vertex source)
    {
        source.minDistance = 0.;
        PriorityQueue<Vertex> vertexQueue = new PriorityQueue<Vertex>();
    vertexQueue.add(source);

    while (!vertexQueue.isEmpty()) {
        Vertex u = vertexQueue.poll();

            // Visit each edge exiting u
            for (Edge e : u.adjacencies)
            {
                Vertex v = e.target;
                double weight = e.weight;
                double distanceThroughU = u.minDistance + weight;
        if (distanceThroughU < v.minDistance) {
            vertexQueue.remove(v);

            v.minDistance = distanceThroughU ;
            v.previous = u;
            vertexQueue.add(v);
        }
            }
        }
    }

    public static List<Vertex> getShortestPathTo(Vertex target)
    {
        List<Vertex> path = new ArrayList<Vertex>();
        for (Vertex vertex = target; vertex != null; vertex = vertex.previous)
            path.add(vertex);

        Collections.reverse(path);
        return path;
    }

    public static ArrayList<String> metroPath(String baslangic,String varis)
    {

        
        Vertex M4 = new Vertex("M4");
        Vertex T1 = new Vertex("T1");
        Vertex M2 = new Vertex("M2");
        Vertex M1 = new Vertex("M1");
        Vertex T4 = new Vertex("T4");
        Vertex MR = new Vertex("MR");
        
        M4.adjacencies = new Edge[]{ new Edge(MR, 8) };
        T1.adjacencies = new Edge[]{ new Edge(MR, 8) ,new Edge(M1, 8),new Edge(T4, 8)};
        M2.adjacencies = new Edge[]{ new Edge(MR, 8) };
        M1.adjacencies = new Edge[]{ new Edge(T1, 8),new Edge(T4, 8) };
        T4.adjacencies = new Edge[]{ new Edge(T1, 8),new Edge(M1, 8) };
        MR.adjacencies = new Edge[]{ new Edge(T1, 8),new Edge(M4, 8),new Edge(M2, 8) };

        List<Vertex> path = null;
        ArrayList<String> test=new ArrayList<String>();

        switch(baslangic){
        case "M4":
        	computePaths(M4);
        	break;
        case "T1":
        	computePaths(T1);
        	break;
        case "M2":
        	computePaths(M2);
        	break;
        case "M1":
        	computePaths(M1);
        	break;
        case "T4":
        	computePaths(T4);
        	break;
        case "MR":
        	computePaths(MR);
        	break;
        
        }
        switch(varis){
        case "M4":
        	path = getShortestPathTo(M4);
        	break;
        case "T1":
        	path = getShortestPathTo(T1);
        	break;
        case "M2":
        	path = getShortestPathTo(M2);
        	break;
        case "M1":
        	path = getShortestPathTo(M1);
        	break;
        case "T4":
        	path = getShortestPathTo(T4);
        	break;
        case "MR":
        	path = getShortestPathTo(MR);
        	break;
        
        }
        for(int i=0;i<path.size();++i){
            test.add(path.get(i).toString());
        }
        return  test;
    }
}