package toplutasima;

import java.util.ArrayList;



/**
 * @author CAGLAR
 * @version 1.0
 * @created 13-Kas-2015 13:22:06
 */
public class Vapur extends Tasitlar {


	private String vapurAdi;
	private String kalkisYeri;
	private int VarisSuresiDk;
	private ArrayList<String> kalkisSaatlerHaftaici;
	private ArrayList<String> kalkisSaatlerCumartesi;
	private ArrayList<String> kalkisSaatlerPazar;


	public Vapur(){
		kalkisSaatlerHaftaici= new ArrayList<String>();
		kalkisSaatlerCumartesi= new ArrayList<String>();
		kalkisSaatlerPazar= new ArrayList<String>();
	}

	public void finalize() throws Throwable {

	}

	/**
	 * 
	 * @param durakAdi
	 */
	public String getVapurAdi() {
		return vapurAdi;
	}

	public void setVapurAdi(String vapurAdi) {
		this.vapurAdi = vapurAdi;
	}

	public int getVarisSuresiDk() {
		return VarisSuresiDk;
	}

	public void setVarisSuresiDk(int varisSuresiDk) {
		VarisSuresiDk = varisSuresiDk;

	}

	public ArrayList<String> getKalkisSaatlerHaftaici() {
		return kalkisSaatlerHaftaici;
	}

	public void setKalkisSaatlerHaftaici(ArrayList<String> kalkisSaatlerHaftaici) {
		this.kalkisSaatlerHaftaici = kalkisSaatlerHaftaici;
	}

	public ArrayList<String> getKalkisSaatlerCumartesi() {
		return kalkisSaatlerCumartesi;
	}

	public void setKalkisSaatlerCumartesi(ArrayList<String> kalkisSaatlerCumartesi) {
		this.kalkisSaatlerCumartesi = kalkisSaatlerCumartesi;
	}

	public ArrayList<String> getKalkisSaatlerPazar() {
		return kalkisSaatlerPazar;
	}

	public void setKalkisSaatlerPazar(ArrayList<String> kalkisSaatlerPazar) {
		this.kalkisSaatlerPazar = kalkisSaatlerPazar;
	}

	public String getKalkisYeri() {
		return kalkisYeri;
	}

	public void setKalkisYeri(String kalkisYeri) {
		this.kalkisYeri = kalkisYeri;
	}
}
