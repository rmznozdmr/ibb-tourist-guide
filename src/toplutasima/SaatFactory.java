package toplutasima;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;



/**
 * @author CAGLAR
 * @version 1.0
 * @created 13-Kas-2015 13:22:03
 */
public class SaatFactory {


	private String Guzergah;
	private String tasitAdi;
	private ArrayList<Vapur> vapurTimes;
	private ArrayList<Metro> metroTimes;

	// VAPURLAR OR METROLAR
	public SaatFactory(String hangiTasit){

		String path;
		vapurTimes= new ArrayList<Vapur>();
		metroTimes = new ArrayList<Metro>();

		if(hangiTasit.equals("METROLAR")) {
			path=new String("../toplutasimason/Tasitlar/METROLAR/");


			try (BufferedReader br = new BufferedReader(new FileReader(path.concat("M4_KADIKOY_KARTAL")))) {

				String sCurrentLine;
				Metro temp=new Metro();
				temp.setBirdurakSuresiDk(3);
				temp.setMetroAdi("M4");
				temp.setTasitAdi("Metro");


				while ((sCurrentLine = br.readLine()) != null) {
					//System.out.println(sCurrentLine);
					String[] durakName;
					durakName=sCurrentLine.split(", ");
					for(int i=0;i<durakName.length;i++){

						temp.getDuraklar().add(durakName[i]);
					}

				}
				metroTimes.add(temp);








			} catch (IOException e) {
				e.printStackTrace();
			}




			try (BufferedReader br = new BufferedReader(new FileReader(path.concat(" T1_Kabataş_Bağcılar")))) {

				String sCurrentLine;
				Metro temp=new Metro();
				temp.setBirdurakSuresiDk(4);
				temp.setMetroAdi("T1");
				temp.setTasitAdi("Metro");


				while ((sCurrentLine = br.readLine()) != null) {
					//System.out.println(sCurrentLine);
					String[] durakName;
					durakName=sCurrentLine.split(", ");
					for(int i=0;i<durakName.length;i++){

						temp.getDuraklar().add(durakName[i]);
					}

				}
				metroTimes.add(temp);



				//for(int i=0;i<metroTimes.get(0).getDuraklar().size();i++)
				//	System.out.println(metroTimes.get(0).getDuraklar().get(i));




			} catch (IOException e) {
				e.printStackTrace();
			}


			try (BufferedReader br = new BufferedReader(new FileReader(path.concat("M1_Aksaray_Atatürk_Havaalanı")))) {

				String sCurrentLine;
				Metro temp=new Metro();
				temp.setBirdurakSuresiDk(4);
				temp.setMetroAdi("M1");
				temp.setTasitAdi("Metro");


				while ((sCurrentLine = br.readLine()) != null) {
					//System.out.println(sCurrentLine);
					String[] durakName;
					durakName=sCurrentLine.split(", ");
					for(int i=0;i<durakName.length;i++){

						temp.getDuraklar().add(durakName[i]);
					}

				}
				metroTimes.add(temp);








			} catch (IOException e) {
				e.printStackTrace();
			}

			try (BufferedReader br = new BufferedReader(new FileReader(path.concat("T4_Topkapı")))) {

				String sCurrentLine;
				Metro temp=new Metro();
				temp.setBirdurakSuresiDk(4);
				temp.setMetroAdi("T4");
				temp.setTasitAdi("Metro");


				while ((sCurrentLine = br.readLine()) != null) {
					//System.out.println(sCurrentLine);
					String[] durakName;
					durakName=sCurrentLine.split(", ");
					for(int i=0;i<durakName.length;i++){

						temp.getDuraklar().add(durakName[i]);
					}

				}
				metroTimes.add(temp);







			} catch (IOException e) {
				e.printStackTrace();
			}


			try (BufferedReader br = new BufferedReader(new FileReader(path.concat("M2")))) {

				String sCurrentLine;
				Metro temp=new Metro();
				temp.setBirdurakSuresiDk(4);
				temp.setMetroAdi("M2");
				temp.setTasitAdi("Metro");


				while ((sCurrentLine = br.readLine()) != null) {
					//System.out.println(sCurrentLine);
					String[] durakName;
					durakName=sCurrentLine.split(", ");
					for(int i=0;i<durakName.length;i++){

						temp.getDuraklar().add(durakName[i]);
					}

				}
				metroTimes.add(temp);








			} catch (IOException e) {
				e.printStackTrace();
			}




			try (BufferedReader br = new BufferedReader(new FileReader(path.concat("MARMARAY")))) {

				String sCurrentLine;
				Metro temp=new Metro();
				temp.setBirdurakSuresiDk(10);
				temp.setMetroAdi("MR");
				temp.setTasitAdi("Metro");


				while ((sCurrentLine = br.readLine()) != null) {
					//System.out.println(sCurrentLine);
					String[] durakName;
					durakName=sCurrentLine.split(", ");
					for(int i=0;i<durakName.length;i++){

						temp.getDuraklar().add(durakName[i]);
					}

				}
				metroTimes.add(temp);








			} catch (IOException e) {
				e.printStackTrace();
			}

		}


		else {

			readVapurTimes("BESIKTAS","USKUDAR",40);
			readVapurTimes("BESIKTAS","KADIKOY",50);

			readVapurTimes("EMINONU","USKUDAR",30);
			readVapurTimes("EMINONU","KADIKOY",40);

			readVapurTimes("KABATAS","KADIKOY",40);


			readVapurTimes("KADIKOY","BESIKTAS",40);
			readVapurTimes("KADIKOY","EMINONU",30);
			readVapurTimes("KADIKOY","KABATAS",40);
			readVapurTimes("KADIKOY","KARAKOY",40);


			readVapurTimes("KARAKOY","KADIKOY",40);


			readVapurTimes("USKUDAR","BESIKTAS",40);
			readVapurTimes("USKUDAR","EMINONU",40);







		}
		
	}

	public void finalize() throws Throwable {

	}

	public String getGuzergah() {
		return Guzergah;
	}

	public void setGuzergah(String guzergah) {
		Guzergah = guzergah;
	}

	public String getTasitAdi() {
		return tasitAdi;
	}

	public void setTasitAdi(String tasitAdi) {
		this.tasitAdi = tasitAdi;
	}

	public ArrayList<Vapur> getVapurTimes() {
		return vapurTimes;
	}

	public void setVapurTimes(ArrayList<Vapur> vapurTimes) {
		this.vapurTimes = vapurTimes;
	}

	public ArrayList<Metro> getMetroTimes() {
		return metroTimes;
	}

	public void setMetroTimes(ArrayList<Metro> metroTimes) {
		this.metroTimes = metroTimes;
	}

	private void readVapurTimes(String location,String destination,int time){


		String
		path=new String("../toplutasimason/Tasitlar/VAPURLAR/");

		Vapur temp=new Vapur();
		temp.setTasitAdi("Vapur");
		path+=location;
		location+="_";

		location+=destination;
		//System.out.println(location);
		temp.setVapurAdi(location);
		temp.getDuraklar().add(destination);
		temp.setVarisSuresiDk(time);

		path+=("/");
		path+=(destination);
		path+=("/");


		try (BufferedReader br = new BufferedReader(new FileReader(path.concat("haftaici.txt")))) {

			String sCurrentLine;
			ArrayList<String> tempSaatler= new ArrayList<String>();

			while ((sCurrentLine = br.readLine()) != null) {
				//System.out.println(sCurrentLine);
				tempSaatler.add(sCurrentLine);
			}
			temp.setKalkisSaatlerHaftaici(tempSaatler);

		} catch (IOException e) {
			e.printStackTrace();
		}

		vapurTimes.add(temp);

	}
}