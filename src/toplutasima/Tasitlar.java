package toplutasima;

import java.util.ArrayList;



/**
 * @author CAGLAR
 * @version 1.0
 * @created 13-Kas-2015 13:22:05
 */
public class Tasitlar {

	private ArrayList<String> duraklar;
	private String tasitAdi;

	public Tasitlar(){
		duraklar = new ArrayList<String>();

	}

	public void finalize() throws Throwable {

	}

	/**
	 *
	 * @param birDurak
	 */
	public int buDurakVarmi(String birDurak){

		for(int i=0;i<getDuraklar().size();i++)
		{
			if(this.getDuraklar().get(i).toLowerCase().contains(birDurak.toLowerCase()))
				return i;
		}
		return -1;
	}

	public ArrayList<String> getDuraklar() {
		return duraklar;
	}

	public void setDuraklar(ArrayList<String> duraklar) {
		this.duraklar = duraklar;
	}

	public String getTasitAdi() {
		return tasitAdi;
	}

	public void setTasitAdi(String tasitAdi) {
		this.tasitAdi = tasitAdi;
	}
}