package toplutasima;

import java.util.ArrayList;
import  java.util.Calendar;

/**
 * @author CAGLAR
 * @version 1.0
 * @created 13-Kas-2015 13:21:59
 */
public class Kullanici {

	private String userType;
	SaatFactory metrolar;
	SaatFactory vapurlar;

	private ArrayList<String> tarihiYerler;
	
	
	private ArrayList<String> TURİSTTİK_YERLER= new ArrayList<String>();
	private ArrayList<String> TURİSTTİK_YERDURAKLAR= new ArrayList<String>();
	 
	private String location;
	private String destination;



	public Kullanici(){
		metrolar= new SaatFactory("METROLAR");
		vapurlar= new SaatFactory("VAPURLAR");
		/*
	    DegisimNoktalari.add("Ayrılık çeşmesi");
	    DegisimNoktalari.add("Sirkeci");
	    DegisimNoktalari.add("Yenikapı");
	    DegisimNoktalari.add("Topkapı");
	    DegisimNoktalari.add("Zeytinburnu");
	    DegisimNoktalari.add("Bağcılar");
		*/
	    ArrayList<String> M4= new ArrayList<String>();
	    ArrayList<String> T1= new ArrayList<String>();
	    ArrayList<String> M2= new ArrayList<String>();
	    ArrayList<String> M1= new ArrayList<String>();
	    ArrayList<String> T4= new ArrayList<String>();
	    ArrayList<String> MR= new ArrayList<String>();
	    M4.add("MR");
	    T1.add("MR");
	    T1.add("M1");
	    T1.add("T4");
	    M2.add("MR");
	    M1.add("T1");
	    M1.add("T4");
	    T4.add("M1");
	    T4.add("T1");
	    MR.add("M4");
	    MR.add("T1");
	    MR.add("M2");
	}

	public String kesisim(String ilk,String iki) {
	String kes="";
		if((ilk.equals("M4")&&iki.equals("MR"))||(ilk.equals("M4")&&iki.equals("MR")) )
		{
			return "Ayrılık çeşmesi";
		}
		if((ilk.equals("MR")&&iki.equals("M2"))||(ilk.equals("M2")&&iki.equals("MR"))){
			return "Yenikapı";
		}
		if((ilk.equals("MR")&&iki.equals("T1"))||(ilk.equals("T1")&&iki.equals("MR"))){
			return "Sirkeci";
		}
		if((ilk.equals("T4")&&iki.equals("T1"))||(ilk.equals("T1")&&iki.equals("T4"))){
			return "Topkapı";
		}
		if((ilk.equals("M1")&&iki.equals("T1"))||(ilk.equals("T1")&&iki.equals("M1"))){
			return "Zeytinburnu";
		}

	return kes;
	}

	public ArrayList<String> getTURİSTTİK_YERLER() {
		return TURİSTTİK_YERLER;
	}

	public void setTURİSTTİK_YERLER(ArrayList<String> TURİSTTİK_YERLER) {
		this.TURİSTTİK_YERLER = TURİSTTİK_YERLER;
	}

	public Kullanici(String userType){
		if (userType.equals("Tourist"))
			this.tourist();
		else
			this.normalUser();
	}

	public void finalize() throws Throwable {

	}

	public void tourist(){

		 TURİSTTİK_YERLER.add("HİPODROM");
		 TURİSTTİK_YERDURAKLAR.add("Sultanahmet");
		 TURİSTTİK_YERLER.add("SULTANAHMET CAMİİ");
		 TURİSTTİK_YERDURAKLAR.add("Sultanahmet");
		 TURİSTTİK_YERLER.add("AYASOFYA");
		 TURİSTTİK_YERDURAKLAR.add("Sultanahmet");
		 TURİSTTİK_YERLER.add("TOPKAPI SARAYI");
		 TURİSTTİK_YERDURAKLAR.add("Sultanahmet");
		 TURİSTTİK_YERLER.add("YEREBATAN SARNICI");
		 TURİSTTİK_YERDURAKLAR.add("Sultanahmet");
		 TURİSTTİK_YERLER.add("KAPALI ÇARŞI(Grand Bazaar)");
		 TURİSTTİK_YERDURAKLAR.add("Beyazıt");
		 TURİSTTİK_YERLER.add("EMİNÖNÜ MEYDANI");
		 TURİSTTİK_YERDURAKLAR.add("Eminönü");
		 TURİSTTİK_YERLER.add("GALATA KULESİ");
		 TURİSTTİK_YERDURAKLAR.add("Şişhane");
		 TURİSTTİK_YERLER.add("DOLMABAHÇE SARAYI");
		 TURİSTTİK_YERDURAKLAR.add("Kabataş");
		 TURİSTTİK_YERLER.add("KIZ KULESİ");
		 TURİSTTİK_YERDURAKLAR.add("Üsküdar");
		 TURİSTTİK_YERLER.add("İSTİKLAL CADDESİ");
		 TURİSTTİK_YERDURAKLAR.add("Taksim");
		 TURİSTTİK_YERLER.add("İSTANBUL ARKEOLOJİ MÜZESİ");
		 TURİSTTİK_YERDURAKLAR.add("Gülhane");
	}

	public void normalUser(){

	}
	
	public String tarihiyerDURAGI(String tarihiyer){
		int index=TURİSTTİK_YERLER.indexOf(tarihiyer);
		if(index!=(-1))
		{
			return TURİSTTİK_YERDURAKLAR.get(index);
		}
		return "";
	}

	public String getLocation() {
		return location;
	}

	public void setLocation(String location) {
		this.location = location;
	}

	public String getDestination() {
		return destination;
	}

	public void setDestination(String destination) {
		this.destination = destination;
	}


	public String getUserType() {
		return userType;
	}

	public void setUserType(String userType) {
		this.userType = userType;
	}

	public SaatFactory getMetrolar() {
		return metrolar;
	}

	public void setMetrolar(SaatFactory metrolar) {
		this.metrolar = metrolar;
	}

	public SaatFactory getVapurlar() {
		return vapurlar;
	}

	public void setVapurlar(SaatFactory vapurlar) {
		this.vapurlar = vapurlar;
	}

	public ArrayList<String> getTarihiYerler() {
		return tarihiYerler;
	}

	public void setTarihiYerler(ArrayList<String> tarihiYerler) {
		this.tarihiYerler = tarihiYerler;
	}

	public String getShortestPath(){

		return null;
	}

	public String findLocation(){
		for(int i=0;i<this.getVapurlar().getVapurTimes().size();i++){
			if(this.getVapurlar().getVapurTimes().get(i).buDurakVarmi(location)!=-1)
				return this.getVapurlar().getVapurTimes().get(i).getDuraklar().get(this.getVapurlar().getVapurTimes().get(i).buDurakVarmi(location));
		}
		for(int i=0;i<this.getMetrolar().getMetroTimes().size();i++){
			if(this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(location)!=-1)
				return this.getMetrolar().getMetroTimes().get(i).getDuraklar().get(this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(location));
		}

		return "Bu durak bulunamadı";

	}

	public String HAT(String durak){
		int locationIndex;
		for(int i=0;i<this.getMetrolar().getMetroTimes().size();i++) {
			locationIndex = this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(durak);
			if (locationIndex != -1) {
				return (this.getMetrolar().getMetroTimes().get(i).getMetroAdi());
			}
		}
		return null;
	}
	

	public int süresi(String baslangic,String varis) {

		int index1, index2, dk = 0;

		for (int i = 0; i < this.getMetrolar().getMetroTimes().size(); i++) {

			if (this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(baslangic) != -1) {

				if (this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(varis) != -1) {
					index1 = this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(baslangic);
					index2 = this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(varis);
					dk = this.getMetrolar().getMetroTimes().get(i).getBirdurakSuresiDk();
					dk = dk * Math.abs(index1 - index2);
				}

			}
		}
			return dk;

	}



	public String YOL(String baslangic,String varis){
		String bas=HAT(baslangic);
		//setLocation(baslangic);
		String hour=this.metroKalkisSaati(baslangic);
		/*if(varisSuresi("metrolar").size()>0)
			hour=varisSuresi("metrolar").get(varisSuresi("metrolar").size()-1);*/
		String var=HAT(varis);
		System.out.println("Kesisim" + kesisim("M4", "MR"));
		ArrayList<String> te=Dijkstra.metroPath(bas, var);
		ArrayList<String> kes=new ArrayList<String>();
		String bas2=baslangic;
		String son2=varis;
		int dakika=0;
		for(int i=0;i<te.size()-1;++i)
		{
			son2=kesisim(te.get(i), te.get(i+1));
			System.out.println(bas2+"--"+son2+"--"+süresi(bas2, son2));
			dakika+=süresi(bas2, son2);

			bas2=son2;

		}
		System.out.println ("BASLANGIC :"+baslangic+" HEDEF: "+varis+"\n" +"BINILECEK METROLAR " + Dijkstra.metroPath(bas,var).toString()  +"da"+dakika);
		return ("<html>BASLANGIC :"+baslangic.toUpperCase()+" HEDEF: "+varis.toUpperCase()+"<br>" +"BINILECEK METROLAR " + Dijkstra.metroPath(bas,var).toString()+ "<br>"+ hour +" Süre: "+dakika +" dk</html>" );
	}
	
	
	public ArrayList<String> varisSuresi(String tasitName){
		ArrayList<String> returnString= new ArrayList<String>();
		int locationIndex;
		int destinationIndex;
		int tempIndex;
		if(tasitName.toLowerCase().equals("metrolar"))
		{
			Calendar now = Calendar.getInstance();
			for(int i=0;i<this.getMetrolar().getMetroTimes().size();i++) {
				tempIndex = this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(location);
				if (tempIndex != -1) {
					locationIndex = tempIndex;
					
					//returnString.add(this.getMetrolar().getMetroTimes().get(i).getDuraklar().get(locationIndex));
					tempIndex=this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(destination);
					if ( tempIndex!= -1) {
						returnString.add(this.getMetrolar().getMetroTimes().get(i).getMetroAdi());
						destinationIndex=tempIndex;
						//returnString.add(this.getMetrolar().getMetroTimes().get(i).getDuraklar().get(this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(destination)));
						String duraklar="";
						if(locationIndex<destinationIndex){
							for(int j=locationIndex;j<=destinationIndex;j++)
							{
								duraklar+=(this.getMetrolar().getMetroTimes().get(i).getDuraklar().get(j));
								duraklar+=", ";
							}
						}
						else
						{
							for(int j=locationIndex;j>=destinationIndex;j--)
							{
								duraklar+=(this.getMetrolar().getMetroTimes().get(i).getDuraklar().get(j));
								duraklar+=", ";
							}
						}
						returnString.add(new String(String.valueOf(Math.abs(locationIndex-destinationIndex)*this.getMetrolar().getMetroTimes().get(i).getBirdurakSuresiDk()))+" dk");
						returnString.add(duraklar);
						if(now.get(Calendar.HOUR_OF_DAY)<6)
							returnString.add("6:00");

						else
						{
							int minute=this.getMetrolar().getMetroTimes().get(i).getBirdurakSuresiDk();

							minute=now.get(Calendar.MINUTE)%minute;

							minute=this.getMetrolar().getMetroTimes().get(i).getBirdurakSuresiDk()-minute;
							minute+=now.get(Calendar.MINUTE);


							returnString.add(now.get(Calendar.HOUR_OF_DAY)+":" +minute + " de kalkiyor");


						}
					}
				}


			}
		}
		else {
			for(int i=0; i<this.getVapurlar().getVapurTimes().size();i++){
				if(this.getVapurlar().getVapurTimes().get(i).getVapurAdi().contains(location.toUpperCase()+"_"+destination.toUpperCase()))
				{
					Calendar now = Calendar.getInstance();

					int chour = now.get(Calendar.HOUR_OF_DAY);
					int cminute = now.get(Calendar.MINUTE);
					int totalMinute=chour*60+cminute;
					//System.out.println(cminute);
					int hourX=24;
					int minuteX=60;
					int hour=24;
					int minute=60;
					int diff=1000000;


					returnString.add(this.getVapurlar().getVapurTimes().get(i).getVapurAdi());
					returnString.add(new String(String.valueOf(this.getVapurlar().getVapurTimes().get(i).getVarisSuresiDk()))+" dk");
					for (int j=0;j<this.getVapurlar().getVapurTimes().get(i).getKalkisSaatlerHaftaici().size();j++){
						String[] time=this.getVapurlar().getVapurTimes().get(i).getKalkisSaatlerHaftaici().get(j).split(":");
						int tempHour=Integer.parseInt(time[0]);
						int tempMinute=Integer.parseInt(time[1]);
						int tempTotalMinute=tempHour*60+tempMinute;

						int tempdiff=tempTotalMinute-totalMinute;
						if(diff>tempdiff && tempdiff>=0){
							diff=tempdiff;
							hour=tempHour;
							minute=tempMinute;
						}




					}
					returnString.add(hour+":"+minute);
				}









			}
		}
		return returnString;
	}

	public String metroKalkisSaati(String locationX) {


		Calendar now = Calendar.getInstance();
		int tempIndex;
		int locationIndex;
		for (int i = 0; i < this.getMetrolar().getMetroTimes().size(); i++) {
			tempIndex = this.getMetrolar().getMetroTimes().get(i).buDurakVarmi(locationX);
			if (tempIndex != -1) {
				locationIndex = tempIndex;
				if (now.get(Calendar.HOUR_OF_DAY) < 6)
					return ("6:00");

				else {
					int minute = this.getMetrolar().getMetroTimes().get(i).getBirdurakSuresiDk();

					minute = now.get(Calendar.MINUTE) % minute;

					minute = this.getMetrolar().getMetroTimes().get(i).getBirdurakSuresiDk() - minute;
					minute += now.get(Calendar.MINUTE);

					if(minute<10)
						return (now.get(Calendar.HOUR_OF_DAY) + ":0" + minute + " de kalkiyor");
					else
						return (now.get(Calendar.HOUR_OF_DAY) + ":" + minute + " de kalkiyor");


				}
			}

		}
		return "6:00";
	}

}