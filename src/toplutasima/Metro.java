package toplutasima;

import java.util.ArrayList;



/**
 * @author CAGLAR
 * @version 1.0
 * @created 13-Kas-2015 13:22:02
 */
public class Metro extends Tasitlar {

	private int BirdurakSuresiDk;
	private String metroAdi;

	public Metro(){

	}

	public void finalize() throws Throwable {

	}

	public int getBirdurakSuresiDk() {
		return BirdurakSuresiDk;
	}

	public void setBirdurakSuresiDk(int birdurakSuresiDk) {
		BirdurakSuresiDk = birdurakSuresiDk;
	}

	public String getMetroAdi() {
		return metroAdi;
	}

	public void setMetroAdi(String metroAdi) {
		this.metroAdi = metroAdi;
	}
}