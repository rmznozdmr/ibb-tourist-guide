/*
 * Created by JFormDesigner on Tue Dec 15 12:21:50 EET 2015
 */

package toplutasima;

import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.IOException;
import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.GroupLayout;
import com.jgoodies.forms.factories.*;

/**
 * @author ramazan ozdemir
 */
public class MainGui extends JFrame {
    private TouristMode tourist= new TouristMode();
    private NormalMode normal= new NormalMode();
    private Image backgroundImage;




    public MainGui()throws IOException  {
        //backgroundImage = ImageIO.read(new File("resim.jpg"));
        //getContentPane().setBackground(Color.WHITE);

        initComponents();
    }





    private void button1ActionPerformed(ActionEvent e) {
        // TODO add your code here
        tourist.setVisible(true);
    }

    private void button2ActionPerformed(ActionEvent e) {
        // TODO add your code here
        normal.setVisible(true);
    }

    private void cancelActionPerformed(ActionEvent e) {
        // TODO add your code here
        dispose();
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - ramazan ozdemir
        DefaultComponentFactory compFactory = DefaultComponentFactory.getInstance();
        button1 = new JButton();
        button2 = new JButton();
        button3 = new JButton();
        title1 = compFactory.createTitle("text");

        //======== this ========
        setForeground(new Color(102, 102, 255));
        setBackground(new Color(0, 51, 51));
        setIconImage(new ImageIcon("/home/discovery/workspace/toplutasima/resim.jpg").getImage());
        setTitle("IBB");
        setName("this");
        Container contentPane = getContentPane();

        //---- button1 ----
        button1.setText("Tourist");
        button1.setFont(new Font("URW Bookman L", Font.PLAIN, 26));
        button1.setForeground(new Color(102, 102, 255));
        button1.setBackground(Color.yellow);
        button1.setVerticalAlignment(SwingConstants.TOP);
        button1.setName("button1");
        button1.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button1ActionPerformed(e);
            }
        });

        //---- button2 ----
        button2.setText("Normal Mode");
        button2.setFont(new Font("Dialog", Font.PLAIN, 26));
        button2.setForeground(new Color(102, 102, 255));
        button2.setBackground(new Color(153, 255, 153));
        button2.setVerticalAlignment(SwingConstants.TOP);
        button2.setName("button2");
        button2.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                button2ActionPerformed(e);
            }
        });

        //---- button3 ----
        button3.setText("CANCEL");
        button3.setFont(new Font("Dialog", Font.BOLD, 16));
        button3.setBackground(new Color(0, 51, 51));
        button3.setForeground(new Color(102, 102, 255));
        button3.setVerticalAlignment(SwingConstants.TOP);
        button3.setName("button3");
        button3.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                cancelActionPerformed(e);
            }
        });

        //---- title1 ----
        title1.setText("IBB UYGULAMASINA HO\u015e GELD\u0130N\u0130Z");
        title1.setFont(new Font("Dialog", Font.BOLD, 20));
        title1.setBackground(new Color(0, 51, 51));
        title1.setForeground(new Color(51, 51, 0));
        title1.setVerticalAlignment(SwingConstants.TOP);
        title1.setHorizontalAlignment(SwingConstants.CENTER);
        title1.setName("title1");

        GroupLayout contentPaneLayout = new GroupLayout(contentPane);
        contentPane.setLayout(contentPaneLayout);
        contentPaneLayout.setHorizontalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(46, 46, 46)
                    .addGroup(contentPaneLayout.createParallelGroup()
                        .addComponent(button1, GroupLayout.PREFERRED_SIZE, 502, GroupLayout.PREFERRED_SIZE)
                        .addComponent(button2, GroupLayout.PREFERRED_SIZE, 502, GroupLayout.PREFERRED_SIZE)
                        .addComponent(title1, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGap(60, 60, 60))
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addGap(203, 203, 203)
                    .addComponent(button3, GroupLayout.PREFERRED_SIZE, 164, GroupLayout.PREFERRED_SIZE)
                    .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        contentPaneLayout.setVerticalGroup(
            contentPaneLayout.createParallelGroup()
                .addGroup(contentPaneLayout.createSequentialGroup()
                    .addContainerGap()
                    .addComponent(title1, GroupLayout.PREFERRED_SIZE, 47, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                    .addComponent(button1, GroupLayout.PREFERRED_SIZE, 63, GroupLayout.PREFERRED_SIZE)
                    .addGap(30, 30, 30)
                    .addComponent(button2, GroupLayout.PREFERRED_SIZE, 67, GroupLayout.PREFERRED_SIZE)
                    .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED, 30, Short.MAX_VALUE)
                    .addComponent(button3, GroupLayout.PREFERRED_SIZE, 48, GroupLayout.PREFERRED_SIZE)
                    .addGap(27, 27, 27))
        );
        pack();
        setLocationRelativeTo(getOwner());
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - ramazan ozdemir
    private JButton button1;
    private JButton button2;
    private JButton button3;
    private JLabel title1;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
